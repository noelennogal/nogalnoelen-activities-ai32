export const formFieldMixin = {
    data() {
      return {
        Search: "",
        Selected: "",
      };
    },
    methods: {
      HideModal() {
        this.$refs.modal.hide();
        this.FormReset();
      },
      FormReset() {
        (this.PatronForm = {
          last_name: "",
          first_name: "",
          middle_name: "",
          email: "",
        }),
          (this.BookForm = {
            name: "",
            author: "",
            copies: 0,
            category_id: "",
          }),
          (this.BorrowReturnForm = {
            patron_id: "",
            copies: 0,
            book_id: "",
          });
        this.Selected = "";
        this.PatronInfo = [];
      },
  
      highlightMatches(text) {
        const matchExists = text
          .toLowerCase()
          .includes(this.Search.toLowerCase());
        if (!matchExists) return text;
  
        const re = new RegExp(this.Search, "ig");
        return text.replace(
          re,
          (matchedText) => `<strong>${matchedText}</strong>`
        );
      },
      AddToast(index) {
        this.$toast.success(`${index} Successfully Added`);
      },
      UpdateToast(index) {
        this.$toast(`${index} Successfully Updated`);
      },
      ErrorToastr() {
  
        this.$toast.error(`Dont Leave blank Input Field`);
      },
      DeleteToaster(){
        this.$toast.error(`Deleted!!!`);
      },
      ExceedToaster(){
        this.$toast.error(`Borrowing  Exceed the Limit!!!`);
      },
      ExceeedToaster(){
        this.$toast.error(`No Books Available Limit!!!`);
      },
      ReturnToaster(){
        this.$toast.error(`Returned Book Exceed the Limit!!!`);
      },
      BorrowerPatrons(arr) {
        if (arr == this.AllPatrons) {
          arr.forEach((patron) => {
            this.PatronInfo.unshift({
              label: `${patron.first_name} ${patron.middle_name} ${patron.last_name}`,
              id: patron.id,
            });
          });
        } else if (arr == this.AllBorrowedBook) {
          arr.forEach((borrower) => {
            if (this.Selected.id == borrower.book_id) {
              this.PatronInfo.unshift({
                label: `${borrower.patron.first_name} ${borrower.patron.middle_name} ${borrower.patron.last_name}`,
                id: borrower.patron_id,
              });
            }
          });
        }
      },
    },
  };
  