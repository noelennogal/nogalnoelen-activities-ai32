<?php

namespace Tests\Unit;

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Borrowed_Books;
use App\Models\Categories;
use App\Models\Patrons;
use App\Models\Returned_Books;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;



class ValidationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */


    public function test_required_category()
    {
        $Category  = [
            'category' => null
        ];

        $response = $this->postJson(route('Categories.store'), $Category);
        $response->assertJsonValidationErrors(['category' => 'Category attribute is required']);
    }
    public function test_minimum_category()
    {

        $Category  = [
            'category' => 'C'
        ];

        $response = $this->postJson(route('Categories.store'), $Category);
        $response->assertJsonValidationErrors(['category' => 'The category must be at least 2 characters.']);
    
    }
    public function test_unique_category()
    {
        Categories::create([
            'category' => 'Computer Science'
        ]);

        $Category  = [
            'category' => 'Computer Science'
        ];

        $response = $this->postJson(route('Categories.store'), $Category);
        $response->assertJsonValidationErrors(['category' => 'Category already exist']);
    
    }

    public function test_required_book_name_author_categoy_id_copies()
    {
       Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = [
            'name' => null, 
            'author' => null,
            'category_id' => null,
            'copies' => null 
        ];

        $response = $this->postJson(route('Books.store'), $Books);
        $response->assertJsonValidationErrors(['name' => 'Name of book is required',
                                            'author' => 'Author of book is required', 
                                            'category_id' => 'A book must belong to a category',
                                            'copies' => 'Copies of book is required']);
       
    }

    public function test_unique_book_name()
    {
        $Category = Categories::create([
            'category' => 'Computer Science'
        ]);

        
        Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ]);

        $Books = [
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => rand(1, 5) 
        ];

        $response = $this->postJson(route('Books.store'), $Books);
        $response->assertJsonValidationErrors(['name' => 'Name of book already exist']);
       
    }

    public function test_book_number_of_copies_is_an_int()
    {
        $Category = Categories::create([
            'category' => 'Computer Science'
        ]);

        $Books = [
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $Category->id,
            'copies' => 'one' 
        ];
        $response = $this->postJson(route('Books.store'), $Books);
        $response->assertJsonValidationErrors(['copies' => 'Number of copies must be integer']);
       
    }

    public function test_book_category_if_it_exist()
    {

        $Books = [
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => 1,
            'copies' => rand(1, 5) 
        ];
        $response = $this->postJson(route('Books.store'), $Books);
        $response->assertJsonValidationErrors(['category_id' => 'Category ID does not exist exist']);
       
    }

    public function test_required_patron_last_name_first_name_middle_name_email()
    {
  
        $Patron = [
            'last_name' => null,
            'first_name' => null,
            'middle_name' => null, 
            'email' => null 
        ];

        $response = $this->postJson(route('Patrons.store'), $Patron);
        $response->assertJsonValidationErrors(['last_name' => 'Last Name of Patron is required',
                                            'first_name' => 'First Name of Patron is required', 
                                            'middle_name' => 'Middle Name of Patron is required',
                                            'email' => 'Email of Patron is required']);
       
    }

    public function test_patron_email_input_if_it_is_valid_email()
    {
        $Patron = [
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Quayzon', 
            'email' => 'licar' 
        ];

        $response = $this->postJson(route('Patrons.store'), $Patron);
        $response->assertJsonValidationErrors(['email' => 'Please input valid email']);
                                           
    }

    public function test_required_borrowed_book_and_patron_info()
    {
  
        $BorrowedBook = [
            'patron_id' => null,
            'copies' => null,
            'book_id' => null
        ];

        $response = $this->postJson(route('Borrowed_Book.store'), $BorrowedBook);
        $response->assertJsonValidationErrors(['patron_id' => 'Patron ID is required',
                                            'copies' => 'Number of Copies is required', 
                                            'book_id' => 'Book ID is required']);
       
    }

    public function test_borrowed_book_book_id_and_patron_id_exist()
    {

        $BorrowedBook = [
            'patron_id' => 1,
            'copies' => rand(1, 5),
            'book_id' => 1 

        ];
        $response = $this->postJson(route('Borrowed_Book.store'), $BorrowedBook);
        $response->assertJsonValidationErrors(['patron_id' => 'Patron ID does not exist exist',
                                                'book_id' => 'Book ID does not exist exist']);
       
    }

    public function test_required_returned_book_and_patron_info()
    {
  
        $ReturnedBook = [
            'patron_id' => null,
            'copies' => null,
            'book_id' => null
        ];

        $response = $this->postJson(route('Returned_Books.store'), $ReturnedBook);
        $response->assertJsonValidationErrors(['patron_id' => 'Patron ID is required',
                                            'copies' => 'Number of Copies is required', 
                                            'book_id' => 'Book ID is required']);
       
    }

    public function test_returned_book_book_id_and_patron_id_exist()
    {

        $ReturnedBook = [
            'patron_id' => 1,
            'copies' => rand(1, 5),
            'book_id' => 1 

        ];
        $response = $this->postJson(route('Returned_Books.store'), $ReturnedBook);
        $response->assertJsonValidationErrors(['patron_id' => 'Patron ID does not exist exist',
                                                'book_id' => 'Book ID does not exist exist']);
       
    }

}
